module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        "learn-blue-dark": "#0033FF",
        "learn-yellow": "#FAFF00",
        "learn-white": "#FFFFFF",
        "learn-black": "#000000",
        "learn-grey": "#EDEDED"
      }
    },
    fontFamily: {
      Gabriel: ["Gabriel, sans-serif"],
      Roboto: ["Roboto, sans-serif"],
    },
    container: {
      center: true,
      padding: "1rem",
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
